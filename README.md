# Site do DevOps Parauapebas

Repositório com o código fonte do site do grupo DevOpsPBS. O site é provido pelo [Hugo](http://gohugo.io/) com tema [Devfest](https://github.com/GDGToulouse/devfest-theme-hugo).

## Instruções para Rodar o Projeto

### Clonar repositório

Com o [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) instalado e devidamente configurado rodar:

```console
git clone https://gitlab.com/devopspbs/devopspbs.org.git
cd devopspbs.org
```

### Usando Docker Compose

Com o [Docker](https://docs.docker.com/install/) e [docker-compose](https://docs.docker.com/compose/install/) instalados e funcionando basta rodar o comando abaixo para subir o ambiente:

```
docker-compose up
```

Rodar o container em background:

```
docker-compose up -d
```

O website fica acessível em http://localhost:1313/

Forçar uma nova construção do container:

```
docker-compose build --no-cache
```

Destruir o ambiente:

```
docker-compose down
```

## License

GPLv3 or later.
