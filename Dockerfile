FROM debian:buster

RUN apt -qqy update && \
    apt -qqy install hugo git && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app

EXPOSE 1313 1313

COPY ./content /app/content
COPY ./data /app/data
COPY ./config.toml /app/config.toml
RUN git clone https://github.com/GDGToulouse/devfest-theme-hugo.git themes/devfest-theme-hugo
RUN cd themes/devfest-theme-hugo && \
    git checkout 75118df462abbbcb714abb38a0100d26a42347e4 && \
    cd ../..
RUN hugo

CMD ["hugo", "server", "--bind", "0.0.0.0"]
