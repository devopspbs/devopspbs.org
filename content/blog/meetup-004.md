---
title: Meetup 004
brief: FLISOL Parauapebas 2019
image: /images/blog/meetup-004.jpeg
date: 2019-04-27
draft: false
---

Um meetup para curtir o maior evento de Software Livre da América Latina que em 2019 conta com uma edição em Parauapebas.

## Programação

https://flisolparauapebas.paralivre.org/

## Repositório

https://gitlab.com/devopspbs/flisol-pbs-2019

## Fotos

https://www.meetup.com/devopspbs/photos/29930162/480824844/

## Patrocínio:

<a href="https://jupiter.com.br/"><img height="90" src="/images/partners/jupiter.svg" alt="Júpiter"></a>
&emsp; <a href="http://pontocompa.com.br/"><img height="90" src="/images/partners/ponto-com.svg" alt="Ponto Com"></a>

## Apoio:

<a href="http://www.parauapebas.pa.gov.br/"><img height="90" src="/images/partners/parauapebas.svg" alt="Parauapebas"></a>
&emsp; <a href="https://parauapebas.ifpa.edu.br/"><img height="90" src="/images/partners/IFPA.svg" alt="IFPA"></a>
&emsp; <a href=""><img height="90" src="/images/partners/paralivre.png" alt="Pará Livre"></a>
&emsp; <a href="https://devopspbs.org/"><img height="90" src="/images/logos/devopspbs-logo-500.png" alt="DevOpsPBS"></a>
