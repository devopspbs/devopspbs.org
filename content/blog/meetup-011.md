---
title: Meetup 011
brief: COVID-19 Meetup Online
image: /images/blog/meetup-011.jpg
date: 2020-04-09
draft: false
---

## Programação

1. [**Abertura**](https://devopspbs.gitlab.io/meetups/meetup-2020-04-09/)

1. **Palestra:**  [Analise de dados com Jupyther Notebook](https://docs.google.com/presentation/d/1_9SadmIyhvmibs1u64VmjRGBZaYaehw2NVLWMkV5uSA/edit#slide=id.gc6f9e470d_0_126)  
   **Autor:** Dhony Silva

1. **Palestra:** [Construindo dashboard's com Vuejs e Django Rest Framework](https://docs.google.com/presentation/d/1_9SadmIyhvmibs1u64VmjRGBZaYaehw2NVLWMkV5uSA/edit#slide=id.g731e0cc593_0_354)  
   **Autor:** Thiago Almeida

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-2020-04-09

## Fotos

https://www.meetup.com/devopspbs/photos/30858803/490076832/
