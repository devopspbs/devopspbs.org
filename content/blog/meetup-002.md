---
title: Meetup 002
brief: Parauapebas Tech Day - Arduino e DB Triggers
image: /images/blog/meetup-002.jpeg
date: 2019-02-23
draft: false
---

## Programação

1. [**Abertura**](https://gitlab.com/devopspbs/meetups/meetup-pbs-tech-day/raw/master/presentations/meetups/meetup-pbs-tech-day.html?inline=false)

1. **Palestra:** Aplicação do Arduino em automação residencial  
   **Autor:** Valmich Rocha, Analista de Sistemas

1. **Palestra:** [Banco de Dados: as Trigger's podem te ajuda ](https://gitlab.com/devopspbs/meetups/meetup-pbs-tech-day/raw/master/As%20Trigger%20Pode%20te%20Ajudar/Banco_de_Dados__As_Trigger_podem_te_ajudar_.pdf?inline=false)  
   **Autor:** Professor Jorge Clésio

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-pbs-tech-day

## Fotos

https://www.meetup.com/devopspbs/photos/29713459/478928576/

## Patrocínio

<a href="http://pontocompa.com.br/"><img height="90" src="/images/partners/ponto-com.svg" alt="Ponto Com"></a>
&emsp; <a href="http://www.nortetelecom.net.br/"><img height="90" src="/images/partners/norte-telecom.png" alt="Norte Telecom"></a>
&emsp; <a href="http://www.nortetecnologia.com.br/"><img height="90" src="/images/partners/norte-tecnologia.svg" alt="Norte Tecnologia"></a>

## Apoio

<a href="http://www.parauapebas.pa.gov.br/"><img height="90" src="/images/partners/parauapebas.svg" alt="Parauapebas"></a>
