---
title: Meetup 015
brief: Meetup Coffee and Beer Tech 1
image: /images/blog/meetup-015.jpg
date: 2022-09-26
draft: false
---

## Programação

1. **Palestra:** Reflexões de um Desenvolvedor sobre IoT  
   **Autor:** Thiago Almeida

1. **Palestra:** Monitoramento de Infraestrutura de TI - Melhores Práticas  
   **Autor:** Thiago Rodrigues

1. **Palestra:** Análise de Dados Públicos com Power BI  
   **Autor:** Dhony Silva

## Fotos

https://www.meetup.com/devopspbs/photos/32785745/
