---
title: Meetup 008
brief: Agile e Personal Branding
image: /images/blog/meetup-008.jpeg
date: 2019-09-27
draft: false
---

Um super meetup para falar sobre a carreira na área da tecnologia. Não perca a chance de conhecer muitas dicas e truques para desenvolver sua visão sistêmica, ser notado e cobiçado pelo mercado de trabalho.

## Programação

1. [**Abertura**](https://devopspbs.gitlab.io/meetups/meetup-2019-09-27/)

1. **Palestra:**  Movimento Agil  
   **Autor:** Ana Sousa

1. **Palestra:**  Visão Sistêmica  
   **Autor:** Thiago Almeida

1. **Palestra:** Personal Branding  
   **Autor:** Dhony Silva

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-2019-09-27

## Fotos

https://www.meetup.com/devopspbs/photos/30390588/485266848/
