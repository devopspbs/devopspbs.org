---
title: Meetup 006
brief: FADESA Tech Day 2019
image: /images/blog/meetup-006.jpg
date: 2019-06-15
draft: false
---

Um meetup para prestigiar o trabalho da galera da comunidade acadêmica da [FADESA](http://fadesa.com.br/) e comunidade [SQL Carajás](https://www.meetup.com/PowerBI-Carajas/) que organizaram um super evento para a comunidade de Tecnologia de Parauapebas e região.

## Inscrições

https://www.sympla.com.br/tech-day-fadesa__529187

## Programação

### Oficinas

1. **Oficina:** Power BI - Introdução a Análise de Dados  
   **Autor:** Dhony Silva

1. **Oficina:** Android - Crie sua App fantástica com Android  
   **Autor:** Willian Begot

### Circuito de Palestras

1. **Palestra:** Desenvolvimento para Jogos com Java EE  
   **Autor:** Glorisnaldo Rosa

1. **Palestra:** IA - Inteligência Artificial. Problema ou Solução?  
   **Autor:** Anderson Costa

1. **Palestra:** MultiCloud  
   **Autor:** Thiago Rodrigues

1. **Palestra:** [A Revolução Industrial da TI](https://tiagorocha.gitlab.io/talk-industrial-revolution-of-it/)  
   **Autor:** Tiago Rocha
