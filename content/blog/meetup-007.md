---
title: Meetup 007
brief: Google Cloud Plataform e Django
image: /images/blog/meetup-007.jpeg
date: 2019-08-31
draft: false
---

Um Super Meetup para falar de Google Cloud Plataform, Python, Django, DevOps e muito mais.

## Programação

1. [**Abertura**](https://devopspbs.gitlab.io/meetups/meetup-2019-08-31/)

1. **Palestra:**  Ecossistema google e o cloud plataforma  
   **Autor:** Thiago Almeida

1. **Palestra:** Como o Django mudou minha vida  
   **Autor:** Valmich Rocha

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-2019-08-31

## Fotos

https://www.meetup.com/devopspbs/photos/30309671/484476326/
