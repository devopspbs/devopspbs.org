---
title: Meetup 005
brief: Docker, DevSecOps, SCRUM e Desenvolvimento Android
image: /images/blog/meetup-005.jpeg
date: 2019-05-25
draft: false
---

## Programação

1. [**Abertura**](https://devopspbs.gitlab.io/meetups/meetup-2019-05-25/)

1. **Palestra:** [Primeiros passos com Docker](https://devopspbs.gitlab.io/talk-intro-docker/)  
   **Autores:**  
   Dhony Silva, Analista de Dados  
   Tiago Rocha, Engenheiro de Operações

1. **Palestra:** O que é o DevSecOps  
   **Autor:** Thiago Rodrigues

1. **Palestra:** SCRUM  
   **Autor:** Hélio Bentzen

1. **Palestra:** Desenvolvimento nativo para Android com Adroid Studio  
   **Autor:** Willian Begot

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-2019-05-25

## Fotos

https://www.meetup.com/devopspbs/photos/30016933/481662668/

## Patrocínio:

<a href="https://jupiter.com.br/"><img height="90" src="/images/partners/jupiter.svg" alt="Júpiter"></a>

## Apoio:

<a href="http://www.parauapebas.pa.gov.br/"><img height="90" src="/images/partners/parauapebas.svg" alt="Parauapebas"></a>
&emsp; <a href="https://parauapebas.ifpa.edu.br/"><img height="90" src="/images/partners/IFPA.svg" alt="IFPA"></a>
