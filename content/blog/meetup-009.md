---
title: Meetup 009
brief: Bots no Telegram e Arquitetura de Apps Mobile
image: /images/blog/meetup-009.jpeg
date: 2019-11-01
draft: false
---

## Programação

1. [**Abertura**](https://devopspbs.gitlab.io/meetups/meetup-2019-11-01/)

1. **Palestra:**  Introdução a bot no Telegram  
   **Autor:** Wilker Paz

1. **Palestra:** Arquitetura de um aplicativo mobile  
   **Autor:** Thiago Almeida

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-2019-11-01

## Fotos

https://www.meetup.com/devopspbs/photos/30491561/486279728/
