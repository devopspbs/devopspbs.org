---
title: Meetup 014
brief: Meetup Online Sobre Docker
image: /images/blog/meetup-014.png
date: 2020-08-18
draft: false
---

## Programação

1. Instalação ao Docker
1. Comandos Básicos Docker
1. Básico de Dockerfile
1. Básico de Docker Compose

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-2020-08-18
