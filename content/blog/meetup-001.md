---
title: Meetup 001
brief: Cloud e IoT
image: /images/blog/meetup-001.jpeg
date: 2019-01-26
draft: false
---

O grupo DevOps PBS, seguindo o seu cronograma de eventos mensais, vai realizar no dia 26 janeiro, sábado, no plenarinho da Câmara Municipal de Parauapebas, um encontro com talks sobre tecnologias emergentes.

## Programação

1. [**Abertura**](https://gitlab.com/devopspbs/meetups/meetup-2019-01-26/raw/master/presentations/meetup-2019-01-26.html?inline=false)

1. **Palestra:** [Industria 4.0 e IoT](https://gitlab.com/devopspbs/meetups/meetup-2019-01-26/raw/master/presentations/Apresenta%C3%A7%C3%A3o.pptx?inline=false)  
   **Autor:** Professor Fernando Cabral
   
1. **Palestra:** [Cloud Computing](https://gitlab.com/devopspbs/meetups/meetup-2019-01-26/raw/master/presentations/talk-cloud.html?inline=false)  
   **Autor:** Tiago Rocha, Engenheiro de Operações

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-2019-01-26

## Fotos

https://www.meetup.com/devopspbs/photos/29628987/
