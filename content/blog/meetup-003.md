---
title: Meetup 003
brief: Análise de Dados, Direito Digital e Comunidade
image: /images/blog/meetup-003.jpeg
date: 2019-03-30
draft: false
---

## Programação

1. [**Abertura**](https://devopspbs.gitlab.io/meetups/meetup-2019-03/)

1. **Palestra:** [Análise de Dados com o Power BI](https://gitlab.com/devopspbs/meetups/meetup-2019-03/raw/master/presentations/01_DevOpsPbs_PowerBI_Analise_de_dados_publicos.pptx?inline=false)  
   **Autor:** Dhony Silva, Analista de Dados

1. **Palestra:** [O Marco Civil da Internet](https://gitlab.com/devopspbs/meetups/meetup-2019-03/raw/master/presentations/Direito%20Digital%20-%20Allan%20Glauber.pptx?inline=false)  
   **Autor:** Allan Glauber, Advogado e Analista de Sistema

1. **Painel:** Comunidade, Eventos e Carreira  
   **Autores:**  
   Dhony Silva, SQL Carajás  
   Luan Coelho, Comunidade PHP-PA  
   Marcivone Reis, Comunidade PHP-PA  
   Tiago Rocha, Comunidade Libre Planet Brasil  
   Thiago Almeida, DevOps Parauapebas  
   Valmich Dias, Comunidade DevOpsPBS  
   William Begot, SQL Carajás

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-2019-03

## Fotos

https://www.meetup.com/devopspbs/photos/29815828/479968672/

## Patrocínio

<a href="https://www.instagram.com/designbrindespa/"><img height="90" src="/images/partners/DESIGNBRINDES.png" alt="Design Brindes"></a>
&emsp; <a href="http://pontocompa.com.br/"><img height="90" src="/images/partners/ponto-com.svg" alt="Ponto Com"></a>
&emsp; <a href="http://www.nortetelecom.net.br/"><img height="90" src="/images/partners/norte-telecom.png" alt="Norte Telecom"></a>
&emsp; <a href="http://www.nortetecnologia.com.br/"><img height="90" src="/images/partners/norte-tecnologia.svg" alt="Norte Tecnologia"></a>

## Apoio

<a href="http://www.parauapebas.pa.gov.br/"><img height="90" src="/images/partners/parauapebas.svg" alt="Parauapebas"></a>
&emsp; <a href="https://parauapebas.ifpa.edu.br/"><img height="90" src="/images/partners/IFPA.svg" alt="IFPA"></a>
