---
title: Meetup 000
brief: Autenticação Segura e Centralizada com Kerberos e LDAP
image: /images/blog/meetup-000.jpg
date: 2018-12-08
draft: false
---

Meetup para troca conhecimento sobre autenticação segura e centralizada com Kerberos e LDAP. Vamos juntar uma equipe de peso para batermos papo e por a mão na massa configurando serviços de autenticação e diretório.

## Repositório

https://gitlab.com/devopspbs/doc/kerberos-manual
