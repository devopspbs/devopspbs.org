---
title: Meetup 010
brief: I Encontro XP Parauapebas
image: /images/blog/meetup-010.jpeg
date: 2020-02-29
draft: false
---

## Programação

1. **Palestra:** Personal Branding  
   **Autor:** Dhony Silva

1. **Palestra:** Empreendedorismo jovem  
   **Autor:** Kaio Cardoso

1. **Palestra:** Carreira em Cloud e no Exterior  
   **Autor:** Nilo Menezes

1. **Palestra:** Sistema SCADA  
   **Autor:** Ayslan Carolli

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-2019-02-29

## Fotos

https://www.meetup.com/devopspbs/photos/30491561/486279728/
