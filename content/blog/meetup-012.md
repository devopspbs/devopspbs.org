---
title: Meetup 012
brief: Linguagem Egua Meetup Online
image: /images/blog/meetup-012.png
date: 2020-04-23
draft: false
---

## Programação

1. [**Abertura**](https://devopspbs.gitlab.io/meetups/meetup-2020-04-23/)

1. **Palestra:** Presente e Futuro da Linguagem Egua  
   **Autores:** Julio Leite e Brennus Caio

## Repositório

https://gitlab.com/devopspbs/meetups/meetup-2020-04-23

## Fotos

https://www.meetup.com/devopspbs/photos/30872155/

## Gravação

https://youtu.be/a3gpPMezeFc
