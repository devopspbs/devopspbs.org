---
title: Meetup 013
brief: Meetup Online Sobre Carreira
image: /images/blog/meetup-013.png
date: 2020-05-21
draft: false
---

## Programação

Bate papo com [Davidson Fellipe](https://www.linkedin.com/in/fellipe/), Engineering Manager na Loadsmart diretamente de Nova York.
