---
title: Code of Conduct

draft: false
---

## 1. Conduta

"Participe de maneira autêntica e ativa. Ao fazer isso, você contribui para a saúde e a longevidade dessa comunidade."
