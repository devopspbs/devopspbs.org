---
title: Home

menu:
  main:
    weight: -1

---


{{% jumbo img="/images/album/2019/_481663198.jpeg" imgLabel="DevOpsPBS" %}}

## Pará - Brasil

<!--
<a class="btn primary btn-lg" style="margin-top: 1em;" href="https://drive.google.com/file/d/1td_9Cr1b2JZvv0bCpOCJNDsEWgVgEp2Y/view?usp=sharing" target="_blank">Become a sponsor</a>
-->

<!--
<a class="btn primary btn-lg" href="https://conference-hall.io/public/event/HJRThubF4uYPkb7jSUxi">
    <svg class="icon icon-cfp"><use xlink:href="#cfp"></use></svg>Submit a presentation
</a>
-->

{{% /jumbo %}}



{{% home-info class="primary" %}}

## O que é DevOps Parauapebas?

É um grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas (Desenvolvedores, Sysadmins, Infosecs, QAs, Engs/Admins de rede, Técnicos de informática, Analistas, Web Designers, etc).
{{% /home-info %}}


{{< youtube-section link="p-bOnV8FRMQ" title="O que é DevOps?" class="" >}}

<!-- ... -->


{{% home-subscribe  class="primary" %}}

<a href="https://meetup.com/devopspbs/"><img height="100" src="/images/logos/Meetup_Logo.png" alt="Meetup"></a>

## Participe dos Nossos Meetups

{{% /home-subscribe %}}

<!-- ... -->


{{% album images="/images/album/2019/_481663198.jpeg,/images/album/2019/_480856290.jpeg,/images/album/2019/_481663300.jpeg,/images/album/2019/_478934322.jpeg,/images/album/2019/_479969638.jpeg,/images/album/2019/_479968775.jpeg,/images/album/2019/_479968697.jpeg,/images/album/2019/_478143115.jpeg" %}}

### Algumas fotos dos nossos meetups.

<a class="btn primary" target="_blank" rel="noopener" href="https://www.meetup.com/devopspbs/photos/">
    Mais fotos
    {{% icon "right" %}}
</a>

{{% /album  %}}

<!-- ... --> 

{{% partners categories="comunidades" %}}

## Parceiros

{{% /partners %}}
