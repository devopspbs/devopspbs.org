---
title: Parceiros
menu:
  main:
    weight: 20

draft: false
---

{{% hero %}}

Se você gostaria de se tornar um patrocinador não exite em fazer contato com um de nossos membros organizadores.

{{% /hero %}}

<!-- Parteners list -->

{{% partners categories="comunidades" %}}

{{% /partners %}}
