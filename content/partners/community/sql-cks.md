---
title: SQL Carajás
type: partner
category: comunidades
website: 'https://www.meetup.com/PowerBI-Carajas/'
logo: /images/partners/community/sql-cks.jpeg
socials: []
---

Encontre SQL Carajás no:

MeetUp: https://www.meetup.com/PowerBI-Carajas/

![Arduino Parauapebas](/images/partners/community/sql-cks.jpeg)
