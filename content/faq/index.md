---
title: FAQs
menu:
  main:
    weight: 80
    
draft: false
---

## Geral

### O que é DevOps Parauapebas?

É um grupo de Entusiastas, Estudantes e Profissionais de TI de Parauapebas (Desenvolvedores, Sysadmins, Infosecs, QAs, Engs/Admins de rede, Técnicos de informática, Analistas, Web Designers, etc).

### Como participar do grupo?

Para participar basta comparecer no local do meetup na data e hora estipulada pela organização. E também se inscrever nos nossos grupos de discussão e mídias sociais.

### O que são meetups?

Meeups são encontros informais organizados mensalmente pelo grupo DevOpsPBS. Sempre com temáticas relacionado a tecnologia, principalmente sobre ferramentas, práticas e técnicas ligadas ao movimento DevOps.

### Quanto eu pago para participar dos meetups?

Nós fazemos o maior esforço para que nossos eventos sejam gratuitos de modo a alcançar o maior número possível de interessados. Entretanto eventualmente poderão ocorrer meetups pagos.

### Tem algum grupo de chat ou rede social do DevOpsPBS?

Sim. Nós estamos nas seguintes plataformas:

* **[GitLab.com](https://gitlab.com/devopspbs/)**  
* **[Instagram](https://instagram.com/devopspbs/)**  
* **[Meetup.com](https://meetup.com/devopspbs/)**  
* **[Telegram](https://t.me/+Rjyir-zmnfRX-x15)**  
* **[WhatsApp](https://chat.whatsapp.com/BkQtXP3CWc8IIUCXxB7JuA)**

## Apresentações

### Onde posso baixar os slides das palestras dos meetups?

Todo o conteúdo usado ou produzido pelo grupo DevOps Parauapebas fica disponível em repositórios do [grupo no GitLab](https://gitlab.com/devopspbs/meetups/).

### Posso participar dos meetups palestrando ou ministrando algum curso, treinamento, oficina, etc?

Sim, nós sempre ficamos muitos felizes em receber interessados em compartilhar conhecimento. Entre em contato com um dos membros organizadores para combinar uma data e horário oportunos.

## Patrocinadores & Parceiros

### Como posso me tornar um patrocinador?

Para se tornar um patrocinador basta entrar em contato com qualquer um dos membros organizadores do grupo DevOps Parauapebas para mais detalhes.

### Como posso me tornar um parceiro do grupo?

Se você faz parte de alguma comunidade ou grupo e gostaria de executar atividades conjuntas entre em contato com um membro organizador para alinhamento de ações.

## Projetos

Todos os projetos do grupo DevOps Parauapebas são desenvolvidos de forma colaborativa e estão disponíveis nos repositórios do [grupo no GitLab](https://gitlab.com/devopspbs/).

### O que eu preciso para contribuir com os projetos do grupo?

Basicamente uma conta do gitlab.com e vontade de participar.

### Quais os conhecimentos e habilidades necessários para contribuir com os projetos?

Você não precisa de nenhum conhecimento ou habilidade especial. Na verdade, participar dos projetos do grupo é uma excelente forma de começar na área de engenharia de software e DevOps. Os membros mais experientes do DevOps Parauapebas ficarão muito felizes em lhe ajudar nos primeiros passos no fantástico mundo da tecnologia.
